
#ifndef DEVICEHUB_PROJECT_H
#define DEVICEHUB_PROJECT_H

#include <Client.h>
#include <DevicehubSensor.h>
#include <Printable.h>

class DevicehubProject : public Printable
{
public:
  DevicehubProject(unsigned long aID, DevicehubSensor* aDatastreams, int aDatastreamsCount);

  virtual size_t printTo(Print&) const;
  unsigned long id() { return _id; };
  int size() { return _datastreamsCount; };
  DevicehubSensor& operator[] (unsigned i) { return _datastreams[i]; };
protected:
  unsigned long _id;
  DevicehubSensor* _datastreams;
  int _datastreamsCount;
};

#endif

