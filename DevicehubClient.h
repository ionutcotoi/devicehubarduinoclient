
#ifndef DEVICEHUBCLIENT_H
#define DEVICEHUBCLIENT_H

#include <Client.h>
#include <DevicehubProject.h>

class DevicehubClient
{
public:
  DevicehubClient(Client& aClient);

  int get(DevicehubProject& aFeed, const char* aApiKey);
  int put(DevicehubProject& aFeed, const char* aApiKey);

protected:
  static const int kCalculateDataLength =0;
  static const int kSendData =1;
  void buildPath(char* aDest, unsigned long projectId);

  Client& _client;
};

#endif
