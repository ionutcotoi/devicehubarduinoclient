#include <SPI.h>
#include <Ethernet.h>
#include <HttpClient.h>
#include <Devicehub.h>

// MAC address for your Ethernet shield
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };

// Your Devicehub project key to let you upload data
char projectKey[] = "YOUR_DEVICEHUB_PROJECT_API_KEY";

// Analog pin which we're monitoring (0 and 1 are used by the Ethernet shield)
int sensorPin = 2;

// Define the strings for our datastream IDs
char sensorId[] = "sensor_reading";
DevicehubSensor sensors[] = {
  DevicehubSensor(sensorId, strlen(sensorId), DATASTREAM_FLOAT),
};
// Finally, wrap the datastreams into a feed
DevicehubProject project(15552, sensors, 1 /* number of datastreams */);

EthernetClient client;
DevicehubClient devicehubClient(client);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  
  Serial.println("Starting single datastream upload to Devicehub...");
  Serial.println();

  while (Ethernet.begin(mac) != 1)
  {
    Serial.println("Error getting IP address via DHCP, trying again...");
    delay(15000);
  }
}

void loop() {
  int sensorValue = analogRead(sensorPin);
  sensors[0].setFloat(sensorValue);

  Serial.print("Read sensor value ");
  Serial.println(sensors[0].getFloat());

  Serial.println("Uploading it to Devicehub");
  int ret = devicehubClient.put(project, projectKey);
  Serial.print("devicehubClient.put returned ");
  Serial.println(ret);

  Serial.println();
  delay(15000);
}
